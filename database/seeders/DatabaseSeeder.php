<?php

namespace Database\Seeders;

use App\Models\Cinema;
use App\Models\Room;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            CountriesTableSeeder::class,
            ArtistsTableSeeder::class,
            MoviesTableSeeder::class,
            CinemasTableSeeder::class,
            RoomsTableSeeder::class,
            SessionsTableSeeder::class,
        ]);

    }
}
