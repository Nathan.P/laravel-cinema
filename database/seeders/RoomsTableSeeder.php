<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('rooms')->insert([
      [
        'places' => 250,
        'cinema_id' => 2
      ], [
        'places' => 100,
        'cinema_id' => 6
      ], [
        'places' => 25,
        'cinema_id' => 5
      ], [
        'places' => 50,
        'cinema_id' => 1
      ], [
        'places' => 49,
        'cinema_id' => 2
      ],
    ]);
  }
}
