<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;

class CountryController extends Controller
{
  /**
   * Class constructor
   */
  public function __construct()
  {
    $this->middleware('ajax')->only('destroy');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('countries.index', ['countries' => Country::paginate(10), 'country' => Country::class]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $this->authorize('create', Country::class);
    
    return view('countries.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CountryRequest $request)
  {
    $data = $request->all();
    $data['user_id'] = Auth::user()->id;

    Country::create($data);

    return redirect()->route('country.index')
      ->with('ok', __('Country has been saved !'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Country $country)
  {
    $this->authorize('update', $country);

    return view('countries.edit', ['country' => $country]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(CountryRequest $request, Country $country)
  {
    $this->authorize('update', $country);

    $country->update($request->all());

    return redirect()->route('country.index')
      ->with('ok', __('Country has been updated !'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Country $country)
  {
    $country->delete();

    return response()->json();
  }
}
