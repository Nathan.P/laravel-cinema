<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
  use HasFactory;

  protected $fillable = ['begin', 'room_id', 'movie_id', 'user_id'];

  public function room()
	{
		return $this->belongsTo(Room::class);
	}

  public function movie()
	{
		return $this->belongsTo(Movie::class);
	}
}
